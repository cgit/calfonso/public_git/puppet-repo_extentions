# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

################################################################################
#Define new facts here. You can later map them to machine types in the         #
#following section.                                                            #
#                                                                              #
# One thing to note is the special syntax allowed when setting default values  #
# for facts.  In the default string you are allowed to use "%repo%" or         #
# "%machine_name%" which will get substituted appropriately whenever this data #
# is used (eg, genome-bootstrap uses this for setting context aware default    #
# values for facts).                                                           #
#                                                                              #
# If a fact needs to be on all machines types set ":on_all_nodes"              #
# appropriately.  Otherwise in the machine declaration section you need to     #
# specify what facts will be used for a particular machine.                    #
################################################################################

# newfact("puppetserver", :on_all_nodes => true) do
#   set_desc "The puppetmaster used for all configuration.  If you are " + 
#            "provisioning a Repo machine, this should be it's FQDN"
#   set_default "%repo%-repo.usersys.redhat.com"
# end

# newfact("logserver") do
#   set_desc "The machine to send syslog output to"
# end


# The facts that start with mysql are used by the MySQL configuration variations.  Each fact has an associated description
# and you can look at the facts for each machine type to get a better understanding of the machine configurations.
newfact("mysql_grade") do
    set_desc "The MySQL grade that you want installed.  The valid values for this fact are 'community' and 'fedora'.  The difference\n" +
             "between community and fedora is that community installs a package called MySQL-*-community and is the latest from MySQL labs\n" +
             "the 'fedora' grade installs the latest mysql package from the fedora distro"
    set_default "community"
end

newfact("mysql_type") do
  set_desc "The type of Mysql server: standalone, primary-master, secondary-master or slave\n" +
           "standalone - A Standalone MySQL server is one that does not require binnary logging\n" +
           "\t\tand has no replication configuration.\n" +
           "primary-master - A Primary Master Mysql server is one that has master to master\n" +
           "\t\treplication configured.  NOTE: you want to add new schemas to the\n" +
           "\t\tprimary master and replicationed to the secondary master.  Be sure\n" +
           "\t\tthe master to master configuration is working.\n" +
           "\t\tsecondary-master - A Secondary Master Mysql server is one that has master to master\n" +
           "\t\treplication configured.  A Secondary Master for our purpose is a\n" +
           "\t\tread only copy of the Primary Master.  All updates will be per-\n" +
           "\t\tformed on the Primary Master and passed to the Secondary Master.\n" +
           "Slave - A Slave Mysql server is one that is connected to a Primary Master server and\n" +
           "\t\tis used for recovery or read-only.\n" +
           "Options: standalone, primiary-master, secondary-master and slave"
  set_default "standalone"
end

newfact("mysql_server_id") do
  set_desc "Server ID for MySQL Replication.  The default is fine if " +
           "you are using a standalone mysql type. If you are creating " +
           "master or slave server the mysql_server_id must be unique. "
  set_default "1"
end

newfact("mysql_master_ip_address") do
  set_desc "MySQL Replication requires the IP address of the Master " +
           "to establish communication between either the Master " +
           "to Slave configuration or a Master to Master configuration."
end

##############################################################################
# Machines definitions go here. It is important that this happens after the  #
# fact declarations.  Aside from that, order does not matter.                #
##############################################################################

# This is used so that all custom machine types run the puppet service.  It is
# completely optional.  If you would like to define how the service is run on
# your machine type feel free to comment this line out and include your own
# class in your custom machine.
classes_on_all_machine_types "genomerepo::client"

MYSQL_BASE_FACTS = %w{ mysql_server_id mysql_grade}
MYSQL_REPLICATION_FACTS = %w{ mysql_master_ip_address }
MYSQL_REPLICATION_DS_FACTS = %w{ mysql_type }

# newmachine("jboss-dev") do
#   include_facts "logserver"
#   set_classes "jboss::dev", "java", "jboss::server::web",
#               "mysql::standalone", "jboss::ds::messaging", "jboss::ds::esb",
#               "apache::ssl", "apache::ajp_http","apache::ajp_devel", 
# end

newmachine("django-example") do
  set_classes "django::server"
end

newmachine("mysql-standalone") do
  include_facts(*MYSQL_BASE_FACTS)
  set_classes "mysql::standalone"
end

# This machine is the master mysql node in a master-to-slave configuration.  You should set this machine before you set up the slave. 
newmachine("mysql-m2s") do
  include_facts(*MYSQL_BASE_FACTS)
  set_classes "mysql::m2s"
end

# This machine is the slave mysql node in a master-to-slave configuration.  You should set this machine after you set up the master. 
newmachine("mysql-slave") do
  include_facts(*MYSQL_BASE_FACTS + MYSQL_REPLICATION_FACTS)
  set_classes "mysql::slave"
end

# This is the machine configuration for *both* sides of a master-to-master configuration.  During bootstrap time, you will be asked
# for appropriate configuration options to setup each of the masters.  When setting up a master to master configuration, you have a bit
# of a "chicken and the egg" situation where you don't yet know the ip address of the machines, and each of the masters need to know
# the ip address of one another.  So, you'll have to update your puppet configuration with the correct ipaddresses *after* these machines
# are provisioned, then run puppet --test on each of the machines in the following sequence:
#
# 1) bootstrap the primary master, expect to see a puppet error due to replication failure (the secondary master doesn't exist yet)
# 2) boostrap the secondary master, you should have entered the correct ip address for the primary so you should not see any puppet errors.
# 3) change the mysql_master_ip_address server parameter for the primary master machine configuration, then run puppetd --test 
#    on the primary master and you should see replication succeed (no errors).
# To verify replication is working from each node do the following:
# 1) connect to mysql as the replication users : msyql -u replication -p (this is setup in your password puppet manifest)
# 2) execute the following: show master status; show slave status \G
newmachine("mysql-m2m") do
  include_facts(*MYSQL_BASE_FACTS + MYSQL_REPLICATION_FACTS)
  set_classes "mysql::m2m"
end

