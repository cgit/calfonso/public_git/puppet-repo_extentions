class genomerepo::appliance {
    file { "/etc/genome/machine_types.rb":
                ensure      => present,
                owner       => "root",
                group       => "root",
                mode        => 0644,
                source      => "puppet:///repo_extensions/machine_types.rb",
                notify      => Service["genomed"],
    }
}
